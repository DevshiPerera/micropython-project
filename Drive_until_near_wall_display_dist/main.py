#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile

# Initialize required objects - brick, motors & ultrasonic sensor 
ev3 = EV3Brick()
left_motor = Motor(Port.B)
right_motor = Motor(Port.C)
eyes = UltrasonicSensor(Port.S1)

# no individual wheel movement required so initialize the drivebase
robot = DriveBase (left_motor, right_motor, wheel_diameter = 55.5, axle_track=104)

# start moving
robot.drive(90,0)

#while hes not too close to a wall display distance 
while eyes.distance(silent=False)>150:
    ev3.screen.clear()
    ev3.screen.draw_text(40,50, eyes.distance(silent=False))
    wait (1000)

#stop moving
robot.stop()
