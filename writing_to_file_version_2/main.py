#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile
import math

# Create your objects here.
ev3 = EV3Brick()

data = DataLog('time', 'distance', name = 'log', timestamp = False)

left_motor = Motor(Port.B)
right_motor = Motor(Port.C)
left_motor.run(50)
right_motor.run(50)

watch = StopWatch()

# Logs the time & distance 10 times
for i in range(10):
    # read angle and time
    angle = left_motor.angle()
    time = watch.time()

    distance = angle*2.75*math.pi/180

    data.log(time, distance)

    # Wait so that motor can move a bit
    wait(1000)

# Now you can upload the file to your computer