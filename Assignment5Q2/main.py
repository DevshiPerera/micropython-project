#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile

# Initialize objects. 
ev3 = EV3Brick()
left_motor = Motor(Port.B)
right_motor = Motor(Port.C)
color = ColorSensor(Port.S4)
eyes = UltrasonicSensor(Port.S1)
robot = DriveBase(left_motor, right_motor, wheel_diameter = 55.5, axle_track=104)

# initialize the important variables
num_blue = 0
max_red = 0.0

# could not find a way to retrive current date and time, importing the datetime library didn't work
# print group name
ev3.screen.print("Group Name")

# wait for a button press and release
while not any(ev3.buttons.pressed()):
    pass
while any(ev3.buttons.pressed()):
    pass

# the robot starts on WHITE, so drive until the color changes
while color.color() == Color.WHITE:
    robot.drive (50,0)

# The robot is now on the colored blocks, do the necessary continuous checks
while color.color() != Color.WHITE:
    
   # current color is blue
    if color.color() == Color.BLUE:

        #tally the number of blue tiles
        num_blue+=1
        # drive with conditional speeds until color changes
        while color.color() == Color.BLUE:
            if eyes.distance(silent=False) < 300:
                robot.drive(10,0)
            else:
                robot.drive(50,0)
   
   
    # current color is RED
    if color.color() == Color.RED:

        #reset robot's distance logging
        robot.reset()
        # drive at a normal speed until color changes
        while color.color() == Color.RED:
            robot.drive(50,0)
        
        # if newly recorded distance is greater than current max_red, replace it
        if float(robot.distance()) > max_red:
            max_red = float(robot.distance())
        
    
# the robot is back on WHITE, stop and output data
robot.stop()
ev3.screen.print("# blue: " + str(num_blue))
ev3.screen.print("red length: " + str(max_red))
# wait for a button press to exit the program so data can be recorded
while not any(ev3.buttons.pressed()):
   pass





