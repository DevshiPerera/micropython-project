#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile

ev3 = EV3Brick()

# Display information on EV3 screen 
ev3.screen.draw_text(10,20, "Group #1")
ev3.screen.draw_text(10,40, "9/24/2021")

# Initialize motors and sensors
gyro = GyroSensor(Port.S4)
left_motor = Motor(Port.B)
right_motor = Motor(Port.C)

# Reset the gyro sensor
if gyro.angle() != 0:
    gyro.reset_angle(0) 

button_count = 0

# Tells the robot to drive
def run_motors(left_speed, right_speed):
    left_motor.run(left_speed)
    right_motor.run(right_speed)

# Stops the robot and increments the number of buttons pressed
def stop_motors():
    left_motor.brake()
    right_motor.brake()
    global button_count
    button_count+=1
    print("button count:", button_count)

# Robot does different things upon each button press
# For no more than 5 button presses
while button_count < 5:
    # Drive backwards for 2 seconds
    if Button.UP in ev3.buttons.pressed():
        run_motors(-50, -50)
        wait(2000)
        stop_motors()
    # rotate clockwise 90 degrees
    elif Button.RIGHT in ev3.buttons.pressed():
        gyro.reset_angle(0)
        while gyro.angle() > -90:
            run_motors(-50, 50)
        stop_motors()
    # rotate counterclockwise 90 degrees
    elif Button.LEFT in ev3.buttons.pressed():
        gyro.reset_angle(0)
        while gyro.angle() < 90:
            run_motors(50, -50)
        stop_motors()

ev3.screen.clear()
ev3.screen.draw_text(20,50, "Program Ended!")
wait(3000)
    
