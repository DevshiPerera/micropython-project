#!/usr/bin/env pybricks-micropython
from pybricks import ev3brick as brick
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile

# no variable type declaration, so nothing preventing someone from changing the value of the const
# you can use capitalization for the convention and to remind yourself not to change the value
US_LOC = 50
# i made the distance from wall a bit longer, sometimes it was running into the wall w/ bumber

# Initialize objects
# you don't need to initialize the buttons
ev3 = EV3Brick()
left_motor = Motor(Port.B)
right_motor = Motor(Port.C)
eyes = UltrasonicSensor(Port.S1)
robot = DriveBase(left_motor, right_motor, wheel_diameter = 55.5, axle_track=104)

#wait for button push and release fucntion
def wait_for_btn():
    while not any(ev3.buttons.pressed()):
        pass
    while any(ev3.buttons.pressed()):
        pass

# drive until too close then wait
# given a speed and distance
def drive_until_near(posPower, distBumper):
    robot.drive(posPower,0)
    while eyes.distance(silent=False) > distBumper + US_LOC:
        pass
    robot.stop()

# no main function in python as far as I know
# just write your "main" outside of other function :)
mpower=80
while mpower>=10: #while speed is greater than or equal to 10
    wait_for_btn()
    drive_until_near(mpower,mpower*2)
    mpower/=2


