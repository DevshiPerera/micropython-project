#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile

# python won't let you reassign outside values in functions aka no pass by reference (details in shared google folder)
# workaround: "return" the values and "reassign" them outside

# Initialize objects
ev3 = EV3Brick()
left_motor = Motor(Port.B)
right_motor = Motor(Port.C)
touch= TouchSensor(Port.S1)
timer = StopWatch()
robot = DriveBase(left_motor, right_motor, wheel_diameter = 55.5, axle_track=104)

# Drives until you hit something, returns time and distance
def driveUntilHit(speed, time, dist):
    robot.drive(speed,0)
    while touch.pressed()==False:
        pass
    robot.stop()

    # you can use built-in Stopwatch and Drivebase classes to track time and distance
    time = timer.time()
    dist = robot.distance()
    
    # return the objects so they can be reassigned outside the fucntion
    return time, dist

# initialize variables
time_ms= 0.0
distance_mm = 0.0

# run function
driveUntilHit(25, time_ms, distance_mm)

#workaround: reassign the variables to get a pass-by-reference result
time_ms, distance_mm = driveUntilHit(25, time_ms, distance_mm)

#print results 
ev3.screen.print("time(ms)=" + str(time_ms))
ev3.screen.print("distance(mm)=" + str(distance_mm))
wait(5000)

