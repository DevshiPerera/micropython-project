#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile
import time

ev3 = EV3Brick()

# Initialize the motors
left_motor = Motor(Port.B)
right_motor = Motor(Port.C)

# Initialize variables
tally = [0,0,0,0,0,0,0,0]
colour = 0

# Robots move forward
left_motor.run(50)
right_motor.run(50)

def string_to_int(colour_sensed):
    if colour_sensed == Color.BLACK:
        return 1
    elif colour_sensed == Color.BLUE:
        return 2
    elif colour_sensed == Color.GREEN:
        return 3
    elif colour_sensed == Color.YELLOW:
        return 4
    elif colour_sensed == Color.RED:
        return 5
    elif colour_sensed == Color.WHITE:
        return 6
    elif colour_sensed == Color.BROWN:
        return 7
    elif colour_sensed == None:
        return 8

while tally[colour] < 25:
    col = ColorSensor(Port.S1)
    colour = string_to_int(col.color())

    # Display colour and tally on screen
    info =  "Colour #" + str(colour) + ": "  + str(tally[colour])
    ev3.screen.clear()
    ev3.screen.draw_text(30,50, info)
    print(tally[colour])

    tally[colour]=tally[colour]+1
    time.sleep(1)

# stop the motors
left_motor.brake()
right_motor.brake()
