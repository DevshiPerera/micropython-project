# Micropython Project

## WHAT CURRENTLY WORKS:

###### Stopping after specified time
###### Button recognition and response
###### Rotations a specified number of degrees (Gyro Sensor)
###### Measuring distance between objects (Ultrasonic sensor)
###### Slow on specified colour (Colour sensor)
###### Stopping after specified distance
###### Stopping after hitting a wall (Touch sensor)
###### Tallying colours
###### Displaying information on screen
###### Timers (stopwatch) and inserting pauses
