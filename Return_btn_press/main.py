#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile

# initilaze objects
ev3 = EV3Brick()
touch = TouchSensor(Port.S1)

#print which button is currently being pressed
def get_btn_press():
    #wait for button press
    while not any(ev3.buttons.pressed()):
        pass
    #print the button being pressed
    if any(ev3.buttons.pressed()):
        ev3.screen.print(str(ev3.buttons.pressed()))
    #wait to give the reader a chance to see the output
    wait(1000)

get_btn_press()

