# First index stores r, g, or b values for pink
# Second index stores r, g, or b values for orange 
red_values = [[],[]]
green_values = [[],[]]
blue_values = [[],[]]

# Reads each line into the arrays
def add_info(index):
    red_values[index].append(f.readline())
    green_values[index].append(f.readline())
    blue_values[index].append(f.readline())

# Gets the average
def average(red_list, green_list, blue_list, ind):
    # Extract the list as integers, not strings
    new_red = [int(i) for i in red_list[ind]]
    new_green = [int(i) for i in green_list[ind]]
    new_blue = [int(i) for i in blue_list[ind]]

    # Calculate the averages
    red_avg = round(sum(new_red) / len(new_red), 2)
    gr_avg = round(sum(new_green) / len(new_green), 2)
    blue_avg = round(sum(new_blue) / len(new_blue), 2)

    # Return as string
    return (str(red_avg), str(gr_avg), str(blue_avg))

# Opens the file if one exists
f = open("output_file.txt", "r")

# Reads the info into the array by giving it an index based on the colour stated by the previous line
while f:
    colour = f.readline()
    if colour == 'Pink\n':
        add_info(0)
    elif colour == 'Orange\n':
        add_info(1)
    elif colour == "":
        break

f.close()

pink_avg = average(red_values, green_values, blue_values, 0)
orange_avg = average(red_values, green_values, blue_values, 1)

# Print all the information! (and get the max and min from built-in functions)
print("The average pink values are: ")
print(pink_avg)
print("The maximum R, G, B values are:\n" + max(red_values[0]) + max(green_values[0]) + max(blue_values[0]))
print("The minimum R, G, B values are: \n" + min(red_values[0]) + min(green_values[0]) + min(blue_values[0]))

print("\nThe average orange values are: ")
print(orange_avg)
print("The maximum R, G, B values are: \n" + max(red_values[1]) + max(green_values[1]) + max(blue_values[1]))
print("The minimum R, G, B values are: \n" + min(red_values[1]) + min(green_values[1]) + min(blue_values[1]))
