#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile

ev3 = EV3Brick()

left_motor = Motor(Port.B)
right_motor = Motor(Port.C)
robot = DriveBase(left_motor, right_motor, wheel_diameter = 55.5, axle_track=104)

# Open an output file that will store data
try:
    f = open('output_file.txt', 'w')
except:
    ev3.screen.draw_text(10, 50, "Error!")

# Configure colour sensor 
colour = ColorSensor(Port.S1)

# Write RGB data to the file!
def write_RGB(paper, redValue, greenValue, blueValue):
    f.write(paper + '\n')
    f.write('%d\n' % (redValue))
    f.write('%d\n' % (greenValue))
    f.write('%d\n' % (blueValue))

def get_reading():
    # The RGB color model is quanitified so that each 
    # Colour component value is a pecentage from 0-100%
    # Instead of a value from 0-255
    return colour.rgb()

# Take RGB reading and discard values
red, green, blue = get_reading()

# Let robot drive
robot.drive(60,0)

# Wait for button press
while not any(ev3.buttons.pressed()):
        pass

stop_program = False

while stop_program == False:
    ev3.buttons.pressed() == None
    if Button.LEFT in ev3.buttons.pressed():
        red, green, blue = get_reading()
        write_RGB("Pink", red, green, blue)
    elif Button.RIGHT in ev3.buttons.pressed():
        red, green, blue = get_reading()
        write_RGB("Orange", red, green, blue)
    elif Button.CENTER in ev3.buttons.pressed():
        stop_program = True

robot.stop()
