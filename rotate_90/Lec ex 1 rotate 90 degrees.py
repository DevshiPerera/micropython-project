#!/usr/bin/env pybricks-micropython

from pybricks.hubs import EV3Brick
from pybricks.ev3devices import Motor
from pybricks.parameters import Port
from pybricks.robotics import DriveBase
from pybricks.ev3devices import TouchSensor, GyroSensor

# Initialize the EV3 Brick.
ev3 = EV3Brick()

# Initialize the motors.
left_motor = Motor(Port.B)
right_motor = Motor(Port.C)

# Initialize the sensors
touch= TouchSensor(Port.S1)
gyro=GyroSensor(Port.S4)

# start turning
left_motor.run(-50)
right_motor.run(50)

# wait to reach 90 degrees or get wacked
while gyro.angle() < 90 and touch.pressed()==False:
    pass

# stop the motors
left_motor.brake()
right_motor.brake()
