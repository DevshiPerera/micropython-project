#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile
import time

ev3 = EV3Brick()

# Initiate Objects
timer = StopWatch()
left_motor = Motor(Port.B)
right_motor = Motor(Port.C)
robot = DriveBase(left_motor, right_motor, wheel_diameter = 55.5, axle_track=104)

try:
    robot.drive(30, 0)
    with open('output_file.txt', 'w') as f:
        time_ms = 0
        while time_ms <= 10000:
            time.sleep(1)

            # Update distance and time
            dist = robot.distance()
            time_ms = timer.time()

            # Write distance to file
            f.write(str(dist))
except:
    ev3.screen.draw_text(10, 50, "Error!")

robot.stop()