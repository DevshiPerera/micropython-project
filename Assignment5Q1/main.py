#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile
import time

ev3 = EV3Brick()

ev3.screen.draw_text(10,20, "Group #1, 8:30")
ev3.screen.draw_text(10,40, "9/24/2021")

# Initialize motors and sensors 
us = UltrasonicSensor(Port.S2)
touch = TouchSensor(Port.S1)
left_motor = Motor(Port.B)
right_motor = Motor(Port.C)

# Setup timer and initial distance
timer = StopWatch()
initial_dist = us.distance(silent=False)

# Waits for the touch sensor to be pressed
# Moves slowly forward until robot hits the wall, then stops
while not touch.pressed():
    left_motor.run(60)
    right_motor.run(60)

if touch.pressed():
    left_motor.stop()
    right_motor.stop()

    # After waiting 2 seconds, robot backs away half the original distance 
    wait(2000)
    while us.distance(silent=False) < (initial_dist/2):
        left_motor.run(-60)
        right_motor.run(-60)

time = timer.time()
ev3.screen.clear()
ev3.screen.draw_text(20,30, "time: "+ str(time) + " ms")
wait(3000)
