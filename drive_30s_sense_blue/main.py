#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile
import time

ev3 = EV3Brick()

# Write your program here. 
ev3.speaker.beep()

start_time = time.time()

# Initialize the motors.
left_motor = Motor(Port.B)
right_motor = Motor(Port.C)

ev3.speaker.beep()

# Initialize the colour sensor
colour = ColorSensor(Port.S1)

# Initialize variables
COLOR_BLUE = 2

# Robots move forward
left_motor.run(50)
right_motor.run(50)

elapsed_time = time.time() - start_time

# wait to reach blue or until 30s pass
while elapsed_time < 30 and colour.color() != Color.BLUE:
    elapsed_time = time.time() - start_time
    pass

# stop the motors
left_motor.brake()
right_motor.brake()

