#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile
import time

ev3 = EV3Brick()

try:
    f = open("demofile.txt", "r")
    num_lines = sum(1 for _ in open('demofile.txt'))
except:
    ev3.screen.draw_text(10, 50, "Error!")
else:
    line = 0
    while line < num_lines*20:
        output = f.readline()
        ev3.screen.draw_text(30, line, output)
        time.sleep(1)
        line=line+20

f.close()