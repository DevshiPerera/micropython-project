#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile

ev3 = EV3Brick()

MED_SPEED = 80
SLOW_SPEED = 20
COLOR_BLUE = 2
COLOR_RED = 5

left_motor = Motor(Port.B)
right_motor = Motor(Port.C)

colour = ColorSensor(Port.S1)
eyes = UltrasonicSensor(Port.S4)

left_motor.run(MED_SPEED)
right_motor.run(MED_SPEED)

while eyes.distance(silent=False) < 4000:
    if colour.color() == Color.RED: 
        left_motor.run(SLOW_SPEED)
        right_motor.run(SLOW_SPEED) 

        while colour.color() == Color.RED and eyes.distance(silent=False) < 4000:
            pass

        left_motor.run(MED_SPEED)
        right_motor.run(MED_SPEED)

left_motor.brake()
right_motor.brake()
