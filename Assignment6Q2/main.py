#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile

ev3 = EV3Brick()

# Configure sensors and motors
us = UltrasonicSensor(Port.S2)
colour = ColorSensor(Port.S4)
left_motor = Motor(Port.B)
right_motor = Motor(Port.C)

# Functions 
def rotate(degrees):
    gyro = GyroSensor(Port.S1)
    while gyro.angle() < degrees:
        left_motor.run(80)
        right_motor.run(-80)
        print(gyro.angle())
    left_motor.brake()
    right_motor.brake()

# Waits for button press and release
def waitOnPress():
    while not any(ev3.buttons.pressed()):
        pass
    while any(ev3.buttons.pressed()):
        pass

# Unless touch sensor is pushed, ultrasonic sensor 
# Searches for objects less than 1m from in front
# Of robot, after which it spins in place for 360 deg.
def monitoring():
    while not any(ev3.buttons.pressed()):
        if us.distance(silent=False) <= 1000:
            rotate(360)
    return False

# Start of Program
ev3.screen.draw_text(10,20, "Group 10")
ev3.screen.draw_text(10,40, "Thursday, 8:30")

waitOnPress()

# Robot drives forward until it detects red
while colour.color() != Color.RED:
    left_motor.run(80)
    right_motor.run(80)

# Rotate counter-clockwise by 90 degrees
rotate(90)

detect_intruders = True

while detect_intruders == True:
    detect_intruders = monitoring()

left_motor.brake()
right_motor.brake()

    
